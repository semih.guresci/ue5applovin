// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ShooterFPSGameMode.generated.h"


struct FSdkConfiguration;

UCLASS(minimalapi)
class AShooterFPSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AShooterFPSGameMode();
	void AttachCallbacks();
};



