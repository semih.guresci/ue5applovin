// Copyright Epic Games, Inc. All Rights Reserved.

#include "ShooterFPSGameMode.h"
#include "ShooterFPSCharacter.h"
#include "UObject/ConstructorHelpers.h"

#include "AppLovinMAX.h"
#include "Public/Constants.h"
#include "Public/DemoLogger.h"



AShooterFPSGameMode::AShooterFPSGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

   /*     UAppLovinMAX::OnSdkInitializedDelegate.AddLambda([this](const FSdkConfiguration& SdkConfiguration)
        {
           
            AttachCallbacks();
        });*/
     //TODO: ADD ID
      UAppLovinMAX::Initialize(SdkKey);

}

void AShooterFPSGameMode::AttachCallbacks()
{
    // Interstitials

    //UAppLovinMAX::OnInterstitialLoadedDelegate.AddLambda([](const FAdInfo& AdInfo)
    //    {
    //        DEMO_LOG("Interstitial loaded");
    //        DEMO_LOG("%s", *AdInfo.ToString());
    //    });

    //UAppLovinMAX::OnInterstitialLoadFailedDelegate.AddLambda([](const FAdInfo& AdInfo, const FAdError& AdError)
    //    {
    //        DEMO_LOG("Interstitial failed to load with error: %s", *AdError.Message);
    //    });

    //UAppLovinMAX::OnInterstitialAdFailedToDisplayDelegate.AddLambda([](const FAdInfo& AdInfo, const FAdError& AdError)
    //    {
    //        DEMO_LOG("Interstitial failed to display with error: %s", *AdError.Message);
    //    });

    //UAppLovinMAX::OnInterstitialClickedDelegate.AddLambda([](const FAdInfo& AdInfo)
    //    {
    //        DEMO_LOG("Interstitial clicked");
    //    });

    //UAppLovinMAX::OnInterstitialHiddenDelegate.AddLambda([](const FAdInfo& AdInfo)
    //    {
    //        DEMO_LOG("Interstitial hidden");
    //    });

    //// Rewarded Ads

    //UAppLovinMAX::OnRewardedAdLoadedDelegate.AddLambda([](const FAdInfo& AdInfo)
    //    {
    //        DEMO_LOG("Rewarded ad loaded");
    //    });

    //UAppLovinMAX::OnRewardedAdLoadFailedDelegate.AddLambda([](const FAdInfo& AdInfo, const FAdError& AdError)
    //    {
    //        DEMO_LOG("Rewarded ad failed to load with error: %s", *AdError.Message);
    //    });

    //UAppLovinMAX::OnRewardedAdFailedToDisplayDelegate.AddLambda([](const FAdInfo& AdInfo, const FAdError& AdError)
    //    {
    //        DEMO_LOG("Rewarded ad failed to display with error: %s", *AdError.Message);
    //    });

    //UAppLovinMAX::OnRewardedAdHiddenDelegate.AddLambda([](const FAdInfo& AdInfo)
    //    {
    //        DEMO_LOG("Rewarded ad hidden");
    //    });

    //UAppLovinMAX::OnRewardedAdClickedDelegate.AddLambda([](const FAdInfo& AdInfo)
    //    {
    //        DEMO_LOG("Rewarded ad clicked");
    //    });

    //UAppLovinMAX::OnRewardedAdReceivedRewardDelegate.AddLambda([](const FAdInfo& AdInfo, const FAdReward& Reward)
    //    {
    //        DEMO_LOG("Rewarded ad received reward: %s", *Reward.ToString());
    //    });

    //// Banner Ads

    //UAppLovinMAX::OnBannerAdLoadedDelegate.AddLambda([](const FAdInfo& AdInfo)
    //    {
    //        DEMO_LOG("Banner loaded");
    //    });

    //UAppLovinMAX::OnBannerAdClickedDelegate.AddLambda([](const FAdInfo& AdInfo)
    //    {
    //        DEMO_LOG("Banner clicked");
    //    });

    //UAppLovinMAX::OnBannerAdLoadFailedDelegate.AddLambda([](const FAdInfo& AdInfo, const FAdError& AdError)
    //    {
    //        DEMO_LOG("Banner ad failed to load with error: %s", *AdError.Message);
    //    });

    //// MRec Ads

    //UAppLovinMAX::OnMRecAdLoadedDelegate.AddLambda([](const FAdInfo& AdInfo)
    //    {
    //        DEMO_LOG("MREC loaded");
    //    });

    //UAppLovinMAX::OnMRecAdClickedDelegate.AddLambda([](const FAdInfo& AdInfo)
    //    {
    //        DEMO_LOG("MREC clicked");
    //    });

    //UAppLovinMAX::OnMRecAdLoadFailedDelegate.AddLambda([](const FAdInfo& AdInfo, const FAdError& AdError)
    //    {
    //        DEMO_LOG("MREC ad failed to load with error: %s", *AdError.Message);
    //    });
}
