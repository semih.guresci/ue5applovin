#pragma once


const FString SdkKey = "";

namespace AdUnitIdentifier
{
#if PLATFORM_IOS
	const FString Banner = "";
	const FString MRec = "";
	const FString Interstitial = "";
	const FString Rewarded = "";
#elif PLATFORM_ANDROID
	const FString Banner = "";
	const FString MRec = "";
	const FString Interstitial = "";
	const FString Rewarded = "";
#else // Empty values for compiling in editor or other platforms
	const FString Banner = "";
	const FString MRec = "";
	const FString Interstitial = "";
	const FString Rewarded = "";
#endif
}
