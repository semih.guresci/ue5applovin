// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ColorfulCube.generated.h"

UCLASS()
class SHOOTERFPS_API AColorfulCube : public AActor
{
	GENERATED_BODY()
			
public:	
	// Sets default values for this actor's properties
	AColorfulCube();
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void OnConstruction(const FTransform& Transform) override;

public:	
	UPROPERTY(EditAnywhere)
		class UMaterialInterface* ParentMaterial;

	UPROPERTY()
		class UMaterialInstanceDynamic* MID;

	UPROPERTY(EditAnywhere)
		FLinearColor NewColor;

	virtual void ChangeColorTo();
};
