// Fill out your copyright notice in the Description page of Project Settings.


#include "ColorfulCube.h"

// Sets default values
AColorfulCube::AColorfulCube()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AColorfulCube::BeginPlay()
{
	Super::BeginPlay();
	PrimaryActorTick.bCanEverTick = false;
}

void AColorfulCube::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	if (ParentMaterial != nullptr) 
	{
		MID = UMaterialInstanceDynamic::Create(ParentMaterial, this);
		//MeshComponent->SetMaterial(0, MID);
	}
}

void AColorfulCube::ChangeColorTo()
{
}


