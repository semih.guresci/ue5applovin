// Fill out your copyright notice in the Description page of Project Settings.


#include "JustADummyCharacter.h"

// Sets default values
AJustADummyCharacter::AJustADummyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AJustADummyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AJustADummyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AJustADummyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

